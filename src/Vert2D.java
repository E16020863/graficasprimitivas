/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Brian
 */
public class Vert2D {
    public int x, y;             
    public int argb;                
    
    public Vert2D(int xval, int yval, int cval)
    {
        x = xval;
        y = yval;
        argb = cval;
    }
}
